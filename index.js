"use strict"

module.exports = {
  isObject: a => (!!a) && (a.constructor === Object),

  isArray: a => (!!a) && (a.constructor === Array),

  isFunc: a => typeof a == 'function'
}